'use strict';
module.exports = function(app) {
  const matchController = require('../controllers/matchController');
  const likeController = require('../controllers/likeController');
  const recommendationCtrl = require('../controllers/recommendationCtrl');
  // matchController Routes
  app.route('/match')
    .get(matchController.list_all_match)

   app.route('/match/:user_id')
    .get(matchController.list_a_user_match)

   app.route('/match')
    .post(matchController.post_a_match)

    app.route('/like/:user_id')
    .get(likeController.list_user_like)

    app.route('/like/:user_id')
    .post(likeController.post_a_like)

    app.route('/recommandation')
    .post(recommendationCtrl.list_all_recommendations)

    app.route('/recommandation/:user_id/caracteristic')
    .post(recommendationCtrl.post_a_user_caracteristic)

    app.route('/recommandation/:user_id/caracteristic')
    .get(recommendationCtrl.get_a_user_caracteristic)

};