process.env.NODE_ENV = 'test'
const dotenv = require('dotenv');
const fs = require('fs');
const envConfig = dotenv.parse(fs.readFileSync('.env.test'))
// Warning will override env overlapping keys
for (const k in envConfig) {
  process.env[k] = envConfig[k]
}

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const sqlFixtures = require('sql-fixtures');
const { expect } = require('chai');
const user = require('../models/User');
const like = require('../models/Like');


var dbConfig = {
        client: 'mysql',
        connection: {
        host     : process.env.DB_HOST,
        user     : process.env.DB_USER,
        port     : process.env.DB_PORT,
        password : process.env.DB_PASS,
        database : process.env.DB_NAME
    }
}
var knex = require('knex')(dbConfig);
var dataSpec = {
  User: [{
    firstName: "Anthony",
    lastName: "Hopkins",
    email: "anthony.hopkins@club-internet.fr",
    created_date: "2018-09-24"
  },
  {
    firstName: "Anthony2",
    lastName: "Hopkins",
    email: "anthony.hopkin@club-internet.fr",
    created_date: "2018-09-24"
  }]
};

const should = chai.should();
chai.use(chaiHttp)
describe('Likes', () => {
  before(async () => {
    await knex('Message').del()
    await knex('Like').del()
    await knex('Match').del()
    await knex('Caracteristic').del()
    await knex('User').del()
    await knex.raw('ALTER TABLE ' + 'User' + ' AUTO_INCREMENT = 1')
    await knex.raw('ALTER TABLE ' + '`Like`' + ' AUTO_INCREMENT = 1')
    await sqlFixtures.create(dbConfig, dataSpec);
  });
    it('should post like', (done) => {
        chai.request(app)
        .post('/like/1')
        .send({
          "user_id": 2,
          "status": 1
        })
        .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
      it('it should get a specific user likes', (done) => {
        chai.request(app)
        .get('/like/1')
        .end((err, res) => {
          console.log(res.body)
          if (res.body) {
            res.should.have.status(200);
            expect(res.body.length).to.equal(1)
            expect(res.body[0].user_id).to.equal(2)
            done();
          }
          });
      });
  });