const { DataTypes, Op } = require("sequelize");
const sequelize = require('../config/dev');
const Match = require('../models/Match.js');
const Message = require('../models/Message.js');
const error_handling = require('../tools/error_handling.js');

const matchInstance = Match(sequelize, DataTypes);

exports.list_all_match = async function(req, res) {
  try {
    var match = await matchInstance.findAll({
    })
    res.json(match);
  } catch (e) {
    error_handling(e, res)
  }
};

exports.post_a_match = async function(req, res) {
  try {
    var match = await matchInstance.create(req.body);
    res.json(match);
  } catch (e) {
    error_handling(e, res);
  }
};

exports.list_a_user_match = async function(req, res) {
  try {
    var message = await matchInstance.findAll({
      where: {
        [Op.or]: [
          { first_user_id: req.params.user_id},
          { second_user_id: req.params.user_id}

        ]
      }
    });
    res.json(message);
  } catch (e) {
    error_handling(e, res)
  }
};

