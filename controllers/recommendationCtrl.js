const { DataTypes, Op } = require("sequelize");
const sequelize = require('../config/dev');
const user = require('../models/User.js');
const photo = require('../models/Photo.js');
const caracteristic = require('../models/Caracteristic.js');
const error_handling = require('../tools/error_handling.js');

const userInstance = user(sequelize, DataTypes);
const caracteristicInstance = caracteristic(sequelize, DataTypes);
const photoInstance = photo(sequelize, DataTypes);

userInstance.hasOne(caracteristicInstance, {
  foreignKey: 'user_id'
});

photoInstance.belongsTo(caracteristicInstance, { foreignKey: "user_id"});
caracteristicInstance.photo = caracteristicInstance.hasOne(photoInstance, { foreignKey: "user_id"});

exports.list_all_recommendations = async function(req, res) {
  try {
    var like = await userInstance.findAll({
      include: {
        nested: true,
        model: caracteristicInstance,
        where: {
          age: {
            [Op.gte]: req.body.age_min,
            [Op.lte]: req.body.age_max,
          },
          gender: req.body.gender
        }
      },
    })
    res.json(like);
  } catch (e) {
    error_handling(e, res)
  }
};

exports.post_a_user_caracteristic = async function(req, res) {
  try {
    req.body.user_id = parseInt(req.params.user_id, 10);
    // req.body.photo.user_id = req.body.user_id;
    // let photo = await photoInstance.create(req.body.photo);
    let caracteristic = await caracteristicInstance.create(req.body, {
      include: [{
        association: caracteristicInstance.photo,
      }]
    });
    res.json(caracteristic);
  } catch (e) {
    error_handling(e, res);
  }
};

exports.get_a_user_caracteristic = async function(req, res) {
  try {
    let caracteristic = await caracteristicInstance.findOne({
      where: {
        user_id: req.params.user_id
      }
    });
    res.json(caracteristic);
  } catch (e) {
    error_handling(e, res);
  }
};